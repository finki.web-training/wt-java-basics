import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CakeShop {
    private List<Cake> cakes;

    public CakeShop() {
        this.cakes = new ArrayList<>();
    }

    public void addQuantityForCake(String name, int quantity) {
        boolean contains = false;
        for (Cake cake : cakes) {
            if (cake.getName().equals(name)) {
                cake.addCakes(quantity);
                contains = true;
            }
        }

        if (!contains) {
            System.out.println("Nema takov vid na torta.");
        } else {
            System.out.println("Dodadeni se " + quantity + " torti od " + name + ".");
        }
    }

    public void sellCakes(String name, int quantity) {
        boolean successfulSale = false;
        for (Cake cake : cakes) {
            if (cake.getName().equals(name) && cake.getQuantity() >= quantity) {
                cake.sellCakes(quantity);
                successfulSale = true;
            }
        }

        if (!successfulSale) {
            System.out.println("Nema dovolna kolicina na torti.");
        } else {
            System.out.println("Prodadeni se " + quantity + " torti od " + name + ".");
        }
    }

    public void print() {
        for (Cake cake : cakes)
            System.out.println(cake);
    }

    public void readCakes(int n, int m) {
        Scanner scanner = new Scanner(System.in);
        int tmp;

        while (n > 0) {
            String line = scanner.nextLine();
            String[] parts = line.split("\\s+");
            if (parts[2].equals(CakeType.TIRAMISU.name()))
                cakes.add(new TiramisuCake(parts[0], Integer.parseInt(parts[1]), CakeType.valueOf(parts[2])));
            else
                cakes.add(new OreoCake(parts[0], Integer.parseInt(parts[1]), CakeType.valueOf(parts[2])));

            tmp = m;
            while (tmp > 0) {
                String line1 = scanner.nextLine();
                String[] parts1 = line1.split("\\s+");

                // 11-12-2021 => parts1[2]
                String[] dateParts = parts1[2].split("-");
                cakes.get(cakes.size() - 1).addItem(new Item(parts1[0],
                        Double.parseDouble(parts1[1]),
                        LocalDate.of(Integer.parseInt(dateParts[2]),
                                Integer.parseInt(dateParts[1]),
                                Integer.parseInt(dateParts[0]))));

                tmp--;
            }
            n--;
        }
    }

    public static void main(String[] args) {
        CakeShop cakeShop = new CakeShop();
        cakeShop.readCakes(3, 2);

        cakeShop.print();
    }
}



