/**
 * TODO: Look at this code. Recall the things explained in class.
 * Answer the following questions:
 * <p>
 * 1. If we have 5 student instances, what is the point value for each student?
 * 2. If we make the point value to be 400 via the instance of the second student, what is the
 * point value for the fourth student?
 * 3. Why do we need the default constructor in this case?
 * 4. Why is there an @Override annotation on some methods? Explain in details.
 * 5. What is Test in this case?
 * 6. What is an interface? What access do we have to an interface?
 */

public class Student implements Test {

    private String name;
    private int age;
    private int numPoints;
    private static int POINT_VALUE = 100;

    public Student() {
    }

    public Student(int age) {
        this.age = age;
    }

    public Student(String name, int age, int numPoints) {
        this.name = name;
        this.age = age;
        this.numPoints = numPoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNumPoints() {
        return numPoints;
    }

    public void setNumPoints(int numPoints) {
        this.numPoints = numPoints;
    }

    @Override
    public String toString() {
        return name + " " + age;
    }

    public boolean canTravel() {
        if (numPoints * POINT_VALUE > 1000)
            return true;
        return false;
    }

    public static void main(String[] args) {
        System.out.println("Message.");
        System.out.print("Message.");
        System.out.printf("%d", 55);

        Student student = new Student("Ana", 56, 900);
        System.out.println(student);
    }

    @Override
    public boolean isTestable() {
        return false;
    }
}