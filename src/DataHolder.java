import java.util.List;

public class DataHolder {

    private List<Person> people;

    public DataHolder() {
        // people = new ArrayList<>();
        people = ReadAndWriteData.readPersonData(3);
    }

    public void simpleDataPrint() {
        for (int i = 0; i < people.size(); i++)
            System.out.println(people.get(i));
    }

    public void forEachDataPrint() {
        for (Person p : people)
            System.out.println(p);
    }

    public void printData() {
        people.forEach(System.out::println);
    }

    public static void main(String[] args) {
        DataHolder dataHolder = new DataHolder();
        System.out.println("SMART PRINT");
        dataHolder.printData();
        System.out.println("SIMPLE PRINT");
        dataHolder.simpleDataPrint();
        System.out.println("FOR EACH PRINT");
        dataHolder.forEachDataPrint();
    }
}
