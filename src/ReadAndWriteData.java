import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadAndWriteData {

    public static void readAndPrintData(int n) {
        Scanner scanner = new Scanner(System.in);

        while (n > 0) {
            /**
             * line = NAME SURNAME AGE
             */
            String line = scanner.nextLine();
            String[] parts = line.split("\\s+");
            Person person = new Person(parts[0], parts[1], Integer.parseInt(parts[2]));
            System.out.println(person);
            n--;

        }
    }

    public static List<Person> readPersonData(int n) {
        Scanner scanner = new Scanner(System.in);
        List<Person> list = new ArrayList<>();

        while (n > 0) {
            String line = scanner.nextLine();
            String[] parts = line.split("\\s+");
            Person person = new Person(parts[0], parts[1], Integer.parseInt(parts[2]));
            list.add(person);
            n--;
        }
        return list;
    }

    /**
     * Read data in format:
     * NAME SURNAME AGE
     * <p>
     * example:
     * Ana Todorovska 88
     * Milan Popovski 67
     * Lile Milevska 99
     */
    public static void main(String[] args) {
        readAndPrintData(3);
    }
}
