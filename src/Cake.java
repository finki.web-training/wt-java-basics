import java.util.ArrayList;
import java.util.List;

public abstract class Cake {

    private String name;
    private int quantity;
    private CakeType type;
    private List<Item> items;
    public static double TAX = 0.05;

    public Cake() {
        items = new ArrayList<>();
    }

    public Cake(String name, int quantity, CakeType type) {
        this.name = name;
        this.quantity = quantity;
        this.type = type;
        items = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void addCakes(int quantity) {
        this.quantity += quantity;
    }

    public void sellCakes(int quantity) {
        this.quantity -= quantity;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public double totalItemsCost() {
        double itemsCost = 0;

        for (Item item : items)
            itemsCost += item.getPrice();
        return itemsCost;
    }

    public abstract double getTotalCakePrice();

    public abstract CakeType getType();

    @Override
    public String toString() {
        return "Cake{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                ", type=" + type +
                '}';
    }
}


