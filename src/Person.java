public class Person {

    /**
     * Class fields can be private, protected, package or public.
     * These are called access modifiers and apply to classes and methods as well.
     * <p>
     * In this class, we have getter and setter methods for all fields,
     * a constructor with all arguments and a toString method override.
     * <p>
     * In Java, every class inherits from the Object class. Therefore, all methods from
     * the Object class are inherited. This is the case with the toString() method,
     * and that is the reason for the @Override annotation usage.
     * <p>
     * If we use a static field or method it works on a class level, i.e.
     * is the same for all class instances (recall the class example).
     */
    private String name;
    private String surname;
    private int age;
    public static int COUNTRY_CODE = 1000;

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("First name: %s Last name: %s Age: %d", name, surname, age);
    }

    public static void main(String[] args) {
        Person person = new Person("A", "T", 44);
        System.out.println(person);
    }
}
