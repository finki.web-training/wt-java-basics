public class TiramisuCake extends Cake {

    public TiramisuCake(String name, int quantity, CakeType type) {
        super(name, quantity, type);
    }

    @Override
    public double getTotalCakePrice() {
        return totalItemsCost() + totalItemsCost() * 0.6 + 120;
    }

    @Override
    public CakeType getType() {
        return CakeType.TIRAMISU;
    }

    @Override
    public String toString() {
        return String.format("CAKE: %s, TYPE: %s, PRICE: %.2f", getName(), getType(), getTotalCakePrice());
    }
}
