public class OreoCake extends Cake {

    public OreoCake(String name, int quantity, CakeType type) {
        super(name, quantity, type);
    }

    @Override
    public double getTotalCakePrice() {
        return totalItemsCost() + totalItemsCost() * 0.5 + 100;
    }

    @Override
    public CakeType getType() {
        return CakeType.OREO;
    }

    @Override
    public String toString() {
        return String.format("CAKE: %s, TYPE: %s, PRICE: %.2f", getName(), getType(), getTotalCakePrice());
    }
}
